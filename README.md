# DialerChat-Ansible-Playbook  
Playbook that installs and launches Dialer-Chat Application.  
### Prerequisites  
* Any Linux based system (Ubuntu, Fedora, etc)  
* [Ansible](https://www.ansible.com/) - software provisioning, configuration management and application-deployment tool  
 
### Getting started  
To run this playbook, we need ansible installed.  
If you want to install it, open terminal (Ubuntu example) and run commands:  
```sh
sudo su  
apt install ansible
```
Make sure that you're in the project folder in your terminal.  
Now we can run playbook file by using command:  
```sh
ansible-playbook playbook.yml -i inventory --connection=local -K
```
Playbook will clone frontend and backend apps, install npm and pm2. Then it will delete previous pm2 app if available, otherwise it will cause an error that will be ignored. At the end it will install all dependencies from package.json files (it may take a while) and then it will start backend server and frontend app.  
Notice that backend app won't have Dialer configured after being cloned, so you will have to configure it manually by entering Dialer's login and password in app.js file.